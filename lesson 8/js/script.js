let input = document.createElement("input");
input.id = "num";
input.value = 10;
input.placeholder = "Enter diameter of circle";

let btnDraw = document.createElement("button");
btnDraw.id = "btn-2";
btnDraw.innerText = "Draw";

let btnDrawCircle = document.getElementById("btn-1");
btnDrawCircle.onclick = function () {
  btnDrawCircle.style.display = "none";
  btnDrawCircle.after(input);
  btnDrawCircle.after(btnDraw);

  btnDraw.onclick = function () {
    btnDraw.style.display = "none";
    input.style.display = "none";

    let diameter = document.getElementById("num").value;

    for (let i = 0; i < 100; i++) {
      let div = document.createElement("div");
      div.className = "circle";

      let content = document.getElementById("num");
      content.after(div);

      div.style.display = "inline-block";
      div.style.width = `${diameter}px`;
      div.style.height = `${diameter}px`;
      div.style.background = `hsl(${Math.floor(
        Math.random() * 360
      )}, 50%, 50%)`;
      div.style.borderRadius = `${diameter}px`;
      div.style.cursor = "pointer";
    }

    let removeCircles = document.querySelectorAll(".circle");
    for (let i = 0; i < removeCircles.length; i++) {
      removeCircles[i].onclick = function () {
        removeCircles[i].remove();
      };
    }
  };
};
