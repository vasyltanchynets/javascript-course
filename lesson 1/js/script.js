var firstname = prompt("Enter your firstname");
var lastname = prompt("Enter your lastname");
var age = prompt("Enter your age");

document.write(
  '<div class="wrapper">' +
    '<header class="header font">' +
    "<span>header 800 * 200</span>" +
    "</header>" +
    '<div class="body">' +
    '<nav class="nav font"><span>nav 100 * 400</span></nav>' +
    '<section class="section font">' +
    "<script>document.write('Your firstname is: ' + firstname)</script> <br>" +
    "<script>document.write('Your lastname is: ' + lastname)</script> <br>" +
    "<script>document.write('Your age is: ' + age)</script>" +
    "</section>" +
    "</div>" +
    '<footer class="footer font"><span>footer 800 * 200</span></footer>' +
    "</div>" +
    "<hr>"
);

//==========================================================================================
document.write("<h1>Arithmetic operations look in the console</h1>");

var x = 6;
var y = 14;
var z = 4;
var res = (x += y - x++ * z); // Postfix increment, multiplication, substraction, operator +=
console.log("x += y - x++ * z equals: " + res);

x = 6;
y = 14;
z = 4;
res = z = --x - y * 5; // Prefix decrement, multiplication, substraction
console.log("z = --x - y * 5 equals: " + res);

x = 6;
y = 14;
z = 4;
res = y /= x + (5 % z); // Operator %, addition, operator /=
console.log("y /= x + 5 % z equals: " + res);

x = 6;
y = 14;
z = 4;
res = z - x++ + y * 5; // Postfix increment, multiplication, substraction, addition
console.log("z - x++ + y * 5 equals: " + res);

x = 6;
y = 14;
z = 4;
res = x = y - x++ * z; // Postfix increment, multiplication, substraction
console.log("x = y - x++ * z equals: " + res);
