const calculator = {
  displayValue: "0",
  firstOperand: null,
  waitingForSecondOperand: false,
  operator: null,
};

function updateDisplay() {
  const display = document.querySelector(".display > input");
  display.value = calculator.displayValue;
}
updateDisplay();

function inputDigit(digit) {
  const { displayValue, waitingForSecondOperand } = calculator;

  if (waitingForSecondOperand === true) {
    calculator.displayValue = digit;
    calculator.waitingForSecondOperand = false;
  } else {
    calculator.displayValue =
      displayValue === "0" ? digit : displayValue + digit;
  }
}

function inputDecimal(dot) {
  if (calculator.waitingForSecondOperand === true) {
    calculator.displayValue = "0.";
    calculator.waitingForSecondOperand = false;
    return;
  }

  if (!calculator.displayValue.includes(dot)) {
    calculator.displayValue += dot;
  }
}

function handleOperator(nextOperator) {
  const { firstOperand, displayValue, operator } = calculator;
  const inputValue = parseFloat(displayValue);

  if (operator && calculator.waitingForSecondOperand) {
    calculator.operator = nextOperator;
    return;
  }

  if (firstOperand == null && !isNaN(inputValue)) {
    calculator.firstOperand = inputValue;
  } else if (operator) {
    const result = calculate(firstOperand, inputValue, operator);

    calculator.displayValue = `${parseFloat(result.toFixed(7))}`;
    calculator.firstOperand = result;
  }

  calculator.waitingForSecondOperand = true;
  calculator.operator = nextOperator;
}

function calculate(firstOperand, secondOperand, operator) {
  if (operator === "+") {
    return firstOperand + secondOperand;
  } else if (operator === "-") {
    return firstOperand - secondOperand;
  } else if (operator === "*") {
    return firstOperand * secondOperand;
  } else if (operator === "/") {
    return firstOperand / secondOperand;
  }

  return secondOperand;
}

function resetCalculator() {
  calculator.displayValue = "0";
  calculator.firstOperand = null;
  calculator.waitingForSecondOperand = false;
  calculator.operator = null;
}

let memoryValue = 0;
const memory = document.querySelector("#memory");

// Write a positive number in memory
function handleMPlus() {
  memoryValue = calculator.displayValue;

  memory.textContent = "m";
  setTimeout(() => {
    memory.textContent = "";
  }, 1000);

  return memoryValue;
}

// Write a negative number in memory
function handleMMinus() {
  memoryValue = -calculator.displayValue;

  memory.textContent = "m";
  setTimeout(() => {
    memory.textContent = "";
  }, 1000);

  return memoryValue;
}

// Display value from memory
function handleMrc() {
  calculator.displayValue = memoryValue;
}

const btn = document.querySelector(".keys");
btn.addEventListener("click", (event) => {
  const { target } = event;
  const { value } = target;
  let isPressBtn = false;

  switch (value) {
    case "+":
    case "-":
    case "*":
    case "/":
    case "=":
      handleOperator(value);
      break;
    case ".":
      inputDecimal(value);
      break;
    case "C":
      resetCalculator();
      break;
    case "m+":
      handleMPlus();
      break;
    case "m-":
      handleMMinus();
      break;
    case "mrc":
      handleMrc();
      break;
    default:
      if (Number.isInteger(parseFloat(value))) {
        inputDigit(value);
      }
  }

  updateDisplay();
});
