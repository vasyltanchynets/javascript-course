let styles = ["Jazz", "Blues"];

styles.push("Rock and roll");
document.write(styles + "<br>");

let middleValue = styles[Math.floor((styles.length - 1) / 2)];
document.write(middleValue + "<br>");

let replaceValue = styles.indexOf(middleValue);
if (replaceValue !== -1) {
  styles[replaceValue] = "Classic";
}
document.write(styles + "<br>");

let deleteFirstElem = styles.shift();
document.write(deleteFirstElem + "<br>");

styles.unshift("Rep", "Reggae");
document.write(styles);
