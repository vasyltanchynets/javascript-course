function CreateNewUser(firstName, lastName, birthday) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.birthday = birthday;
}

CreateNewUser.prototype.getLogin = function () {
  let firstNameLetter = this.firstName.charAt(0).toLowerCase();
  return firstNameLetter + this.lastName.toLowerCase();
};

CreateNewUser.prototype.getAge = function () {
  let date = new Date();
  return date.getFullYear() - this.birthday.slice(6);
};

CreateNewUser.prototype.getPassword = function () {
  return (
    this.firstName.charAt(0).toUpperCase() +
    this.lastName.toLowerCase() +
    this.birthday.slice(6)
  );
};

const newUser = new CreateNewUser(
  prompt("Enter your name:", "Jason"),
  prompt("Enter your surname:", "Statham"),
  prompt("Enter your birthday dd.mm.yyyy:", "26.07.1967")
);

document.write("Login: " + newUser.getLogin() + "<br>");
document.write("Age: " + newUser.getAge() + "<br>");
document.write("Password: " + newUser.getPassword() + "<br>");

document.write("<hr>");

//==========================================================================================
function FilterBy(arr, type) {
  this.arr = arr;
  this.type = type;
}

FilterBy.prototype.getArray = function () {
  let newMass = this.arr.filter((item) => typeof item !== this.type);
  return newMass;
};

const arr = new Array("hello", "world", 23, "23", null);

let filterArr = new FilterBy(arr, "string");
let newArr = filterArr.getArray();
newArr.forEach((i) => document.write(i + " "));
