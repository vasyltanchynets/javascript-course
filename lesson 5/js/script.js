const doc = {
  title: "JavaScript",
  body: "Objects",
  footer: "copyright",
  date: "13.06.2022",

  app: {
    title: {
      name: "GTA 5",
    },
    body: {
      country: "USA",
    },
    footer: {
      brand: "Rockstar Games",
    },
    date: {
      released: "17.09.2017",
    },
  },

  showDoc: function () {
    document.write(
      `Document's title - '${this.title}', body - '${this.body}', footer - '${this.footer}', 
      date - '${this.date}' 
      <br> 
      Application's name - '${this.app.title.name}', body - '${this.app.body.country}', 
      footer - '${this.app.footer.brand}', date - '${this.app.date.released}'`
    );
  },
};

doc.showDoc();
