let array = [1, 2, 3, 4, 5];

const fn = (arr) => {
  let mass = [];

  for (let i = 0; i < arr.length; i++) {
    mass[i] = arr[i] * 10;
  }

  return mass;
};

const map = (array, callback) => {
  document.write(`array: ${array} <br>`);
  document.write(`changed array: ${callback(array)}`);
};
map(array, fn);

document.write("<hr>");

//==========================================================================================
let age = prompt("How old are you?", 24);

const checkAge = (age) => {
  return age > 18 ? document.write(true) : confirm("Parents allowed?");
};
checkAge(age);
