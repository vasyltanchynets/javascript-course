// Rectangle
for (let i = 0; i < 20; i++) {
  document.write("* ");
}

for (let i = 0; i < 5; i++) {
  document.write("<br>");
  document.write("*");
  for (let j = 0; j < 55; j++) {
    document.write("&nbsp;");
  }
  document.write("*");
}

document.write("<br>");
for (let i = 0; i < 20; i++) {
  document.write("* ");
}
document.write("<br><br>");

// Right triangle
var n = 8;

for (let i = 0; i <= n; i++) {
  for (let j = 1; j <= i; j++) {
    if (j == 1 || j == i || i == n) {
      document.write("* ");
    } else {
      document.write("&nbsp;&nbsp;&nbsp;");
    }
  }
  document.write("<br>");
}
document.write("<br><br>");

// Equilateral triangle
for (let i = 1; i <= n; i++) {
  for (let j = i; j < n; j++) {
    document.write("&nbsp;&nbsp;&nbsp;");
  }

  for (let j = 1; j <= 2 * i - 1; j++) {
    if (i == n || j == 1 || j == 2 * i - 1) {
      document.write("* ");
    } else {
      document.write("&nbsp;&nbsp;&nbsp;");
    }
  }
  document.write("<br>");
}
document.write("<br><br>");

// Diamond
n = 6;
let z = 1;
for (let i = 0; i <= n; i++) {
  for (let j = n; j > i; j--) {
    document.write("&nbsp;&nbsp;&nbsp;");
  }

  document.write("* ");

  if (i > 0) {
    for (let k = 1; k <= z; k++) {
      document.write("&nbsp;&nbsp;&nbsp;");
    }
    z += 2;
    document.write("* ");
  }
  document.write("<br>");
}

z -= 4;
for (let i = 0; i <= n - 1; i++) {
  for (let j = 0; j <= i; j++) {
    document.write("&nbsp;&nbsp;&nbsp;");
  }

  document.write("* ");

  for (let k = 1; k <= z; k++) {
    document.write("&nbsp;&nbsp;&nbsp;");
  }

  z -= 2;

  if (i != n - 1) {
    document.write("* ");
  }
  document.write("<br>");
}

//==========================================================================================
document.write("<hr>");

let a = parseFloat(prompt("Enter number a:", "5"));
let b = parseFloat(prompt("Enter number b:", "7"));

let res = a + b < 4 ? "Little" : "Much";
document.write(res);

//==========================================================================================
document.write("<hr>");

const login = prompt("Enter login:", "Vasyl");
const message =
  login == "Vasyl"
    ? "Hello"
    : login == "Director"
    ? "Welcome"
    : login == ""
    ? "Doesn't have login"
    : "";

document.write(message);

//==========================================================================================
document.write("<hr>");

let A = parseInt(prompt("Enter number A:", "2"));
let B = parseInt(prompt("Enter number B:", "10"));

let sum = 0;

for (let i = A; i <= B; i++) {
  if (A < B) {
    sum += i;
  }
}

if (A < B) {
  document.write(sum);
} else {
  document.write("Does not appropriate condition of the problem");
}
