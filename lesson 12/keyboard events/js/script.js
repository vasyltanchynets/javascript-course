let body = document.querySelector("body");
let btnEnter = document.querySelector(".btn-enter");
let btnS = document.querySelector(".btn-s");
let btnE = document.querySelector(".btn-e");
let btnO = document.querySelector(".btn-o");
let btnN = document.querySelector(".btn-n");
let btnL = document.querySelector(".btn-l");
let btnZ = document.querySelector(".btn-z");

body.addEventListener("keydown", function (e) {
  console.log(`Key down - ${e.key}`);

  switch (true) {
    case e.key === "Enter":
      {
        btnEnter.style.backgroundColor = "blue";

        if (btnS.style.backgroundColor == "blue") {
          btnS.style.backgroundColor = "black";
        } else if (btnE.style.backgroundColor == "blue") {
          btnE.style.backgroundColor = "black";
        } else if (btnO.style.backgroundColor == "blue") {
          btnO.style.backgroundColor = "black";
        } else if (btnN.style.backgroundColor == "blue") {
          btnN.style.backgroundColor = "black";
        } else if (btnL.style.backgroundColor == "blue") {
          btnL.style.backgroundColor = "black";
        } else if (btnZ.style.backgroundColor == "blue") {
          btnZ.style.backgroundColor = "black";
        }
      }
      break;
    case e.key === "s" || e.key === "S":
      {
        btnS.style.backgroundColor = "blue";

        if (btnEnter.style.backgroundColor == "blue") {
          btnEnter.style.backgroundColor = "black";
        } else if (btnE.style.backgroundColor == "blue") {
          btnE.style.backgroundColor = "black";
        } else if (btnO.style.backgroundColor == "blue") {
          btnO.style.backgroundColor = "black";
        } else if (btnN.style.backgroundColor == "blue") {
          btnN.style.backgroundColor = "black";
        } else if (btnL.style.backgroundColor == "blue") {
          btnL.style.backgroundColor = "black";
        } else if (btnZ.style.backgroundColor == "blue") {
          btnZ.style.backgroundColor = "black";
        }
      }
      break;
    case e.key === "e" || e.key === "E":
      {
        btnE.style.backgroundColor = "blue";

        if (btnEnter.style.backgroundColor == "blue") {
          btnEnter.style.backgroundColor = "black";
        } else if (btnS.style.backgroundColor == "blue") {
          btnS.style.backgroundColor = "black";
        } else if (btnO.style.backgroundColor == "blue") {
          btnO.style.backgroundColor = "black";
        } else if (btnN.style.backgroundColor == "blue") {
          btnN.style.backgroundColor = "black";
        } else if (btnL.style.backgroundColor == "blue") {
          btnL.style.backgroundColor = "black";
        } else if (btnZ.style.backgroundColor == "blue") {
          btnZ.style.backgroundColor = "black";
        }
      }

      break;
    case e.key === "o" || e.key === "O":
      {
        btnO.style.backgroundColor = "blue";

        if (btnEnter.style.backgroundColor == "blue") {
          btnEnter.style.backgroundColor = "black";
        } else if (btnS.style.backgroundColor == "blue") {
          btnS.style.backgroundColor = "black";
        } else if (btnE.style.backgroundColor == "blue") {
          btnE.style.backgroundColor = "black";
        } else if (btnN.style.backgroundColor == "blue") {
          btnN.style.backgroundColor = "black";
        } else if (btnL.style.backgroundColor == "blue") {
          btnL.style.backgroundColor = "black";
        } else if (btnZ.style.backgroundColor == "blue") {
          btnZ.style.backgroundColor = "black";
        }
      }
      break;
    case e.key === "n" || e.key === "N":
      {
        btnN.style.backgroundColor = "blue";

        if (btnEnter.style.backgroundColor == "blue") {
          btnEnter.style.backgroundColor = "black";
        } else if (btnS.style.backgroundColor == "blue") {
          btnS.style.backgroundColor = "black";
        } else if (btnE.style.backgroundColor == "blue") {
          btnE.style.backgroundColor = "black";
        } else if (btnO.style.backgroundColor == "blue") {
          btnO.style.backgroundColor = "black";
        } else if (btnL.style.backgroundColor == "blue") {
          btnL.style.backgroundColor = "black";
        } else if (btnZ.style.backgroundColor == "blue") {
          btnZ.style.backgroundColor = "black";
        }
      }
      break;
    case e.key === "l" || e.key === "L":
      {
        btnL.style.backgroundColor = "blue";

        if (btnEnter.style.backgroundColor == "blue") {
          btnEnter.style.backgroundColor = "black";
        } else if (btnS.style.backgroundColor == "blue") {
          btnS.style.backgroundColor = "black";
        } else if (btnE.style.backgroundColor == "blue") {
          btnE.style.backgroundColor = "black";
        } else if (btnO.style.backgroundColor == "blue") {
          btnO.style.backgroundColor = "black";
        } else if (btnN.style.backgroundColor == "blue") {
          btnN.style.backgroundColor = "black";
        } else if (btnZ.style.backgroundColor == "blue") {
          btnZ.style.backgroundColor = "black";
        }
      }
      break;
    case e.key === "z" || e.key === "Z":
      {
        btnZ.style.backgroundColor = "blue";

        if (btnEnter.style.backgroundColor == "blue") {
          btnEnter.style.backgroundColor = "black";
        } else if (btnS.style.backgroundColor == "blue") {
          btnS.style.backgroundColor = "black";
        } else if (btnE.style.backgroundColor == "blue") {
          btnE.style.backgroundColor = "black";
        } else if (btnO.style.backgroundColor == "blue") {
          btnO.style.backgroundColor = "black";
        } else if (btnN.style.backgroundColor == "blue") {
          btnN.style.backgroundColor = "black";
        } else if (btnL.style.backgroundColor == "blue") {
          btnL.style.backgroundColor = "black";
        }
      }
      break;
  }
});
