function makePizza() {
  let smallPizza = document.getElementById("small");
  let midPizza = document.getElementById("mid");
  let bigPizza = document.getElementById("big");

  let priceGeneral = document.createElement("p");
  let priceSouces = document.createElement("p");
  let priceTopings = document.createElement("p");

  let price = document.querySelector(".price > p");
  let sauces = document.querySelector(".sauces > p");
  let topings = document.querySelector(".topings > p");

  let pPizza = 0;
  let sPizza = 0;
  let tPizza = 0;

  let sauceClassic = document.getElementById("sauceClassic");
  let sauceBBQ = document.getElementById("sauceBBQ");
  let sauceRikotta = document.getElementById("sauceRikotta");
  let moc1 = document.getElementById("moc1");
  let moc2 = document.getElementById("moc2");
  let moc3 = document.getElementById("moc3");
  let telya = document.getElementById("telya");
  let vetch1 = document.getElementById("vetch1");
  let vetch2 = document.getElementById("vetch2");

  let target = document.querySelector(".table");

  let draggable = document.querySelectorAll(".draggable");

  let ingridients = document.querySelector(".ingridients");
  let choseIngredient = "";

  ingridients.addEventListener("mousedown", (e) => {
    choseIngredient = e.target.id;
  });

  draggable.forEach((img) => {
    img.addEventListener("dragstart", function () {
      this.style.border = "2px dotted #000";
    });

    img.addEventListener("dragend", function () {
      this.style.border = "";
    });
  });

  target.addEventListener("dragenter", function (e) {
    this.style.border = "2px solid silver";
  });

  target.addEventListener("dragleave", function () {
    this.style.border = "";
  });

  target.addEventListener("dragover", function (e) {
    if (e.preventDefault) e.preventDefault();
    return false;
  });

  target.addEventListener("drop", function (e) {
    if (e.preventDefault) e.preventDefault();
    if (e.stopPropagation) e.stopPropagation();

    this.style.border = "";

    if (choseIngredient == sauceClassic.id) {
      let div = document.createElement("img");
      div.src = "img/03.png";

      this.appendChild(div);

      if (smallPizza.checked) {
        priceSouces.innerText = `${(sPizza += 30)}₴`;
        sauces.after(priceSouces);

        priceGeneral.innerText = `${(pPizza = sPizza)}₴`;
        price.after(priceGeneral);
      }

      if (midPizza.checked) {
        priceSouces.innerText = `${(sPizza += 35)}₴`;
        sauces.after(priceSouces);

        priceGeneral.innerText = `${(pPizza = sPizza)}₴`;
        price.after(priceGeneral);
      }

      if (bigPizza.checked) {
        priceSouces.innerText = `${(sPizza += 40)}₴`;
        sauces.after(priceSouces);

        priceGeneral.innerText = `${(pPizza = sPizza)}₴`;
        price.after(priceGeneral);
      }

      return false;
    }

    if (choseIngredient == sauceBBQ.id) {
      let div = document.createElement("img");
      div.src = "img/04.png";

      this.appendChild(div);

      if (smallPizza.checked) {
        priceSouces.innerText = `${(sPizza += 35)}₴`;
        sauces.after(priceSouces);

        priceGeneral.innerText = `${(pPizza = sPizza)}₴`;
        price.after(priceGeneral);
      }

      if (midPizza.checked) {
        priceSouces.innerText = `${(sPizza += 40)}₴`;
        sauces.after(priceSouces);

        priceGeneral.innerText = `${(pPizza = sPizza)}₴`;
        price.after(priceGeneral);
      }

      if (bigPizza.checked) {
        priceSouces.innerText = `${(sPizza += 45)}₴`;
        sauces.after(priceSouces);

        priceGeneral.innerText = `${(pPizza = sPizza)}₴`;
        price.after(priceGeneral);
      }

      return false;
    }

    if (choseIngredient == sauceRikotta.id) {
      let div = document.createElement("img");
      div.src = "img/05.png";

      this.appendChild(div);

      if (smallPizza.checked) {
        priceSouces.innerText = `${(sPizza += 38)}₴`;
        sauces.after(priceSouces);

        priceGeneral.innerText = `${(pPizza = sPizza)}₴`;
        price.after(priceGeneral);
      }

      if (midPizza.checked) {
        priceSouces.innerText = `${(sPizza += 43)}₴`;
        sauces.after(priceSouces);

        priceGeneral.innerText = `${(pPizza = sPizza)}₴`;
        price.after(priceGeneral);
      }

      if (bigPizza.checked) {
        priceSouces.innerText = `${(sPizza += 48)}₴`;
        sauces.after(priceSouces);

        priceGeneral.innerText = `${(pPizza = sPizza)}₴`;
        price.after(priceGeneral);
      }

      return false;
    }

    if (choseIngredient == moc1.id) {
      let div = document.createElement("img");
      div.src = "img/06.png";

      this.appendChild(div);

      if (smallPizza.checked) {
        priceTopings.innerText = `${(tPizza += 42)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (midPizza.checked) {
        priceTopings.innerText = `${(tPizza += 47)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (bigPizza.checked) {
        priceTopings.innerText = `${(tPizza += 52)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      return false;
    }

    if (choseIngredient == moc2.id) {
      let div = document.createElement("img");
      div.src = "img/07.png";

      this.appendChild(div);

      if (smallPizza.checked) {
        priceTopings.innerText = `${(tPizza += 44)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (midPizza.checked) {
        priceTopings.innerText = `${(tPizza += 49)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (bigPizza.checked) {
        priceTopings.innerText = `${(tPizza += 54)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      return false;
    }

    if (choseIngredient == moc3.id) {
      let div = document.createElement("img");
      div.src = "img/08.png";

      this.appendChild(div);

      if (smallPizza.checked) {
        priceTopings.innerText = `${(tPizza += 47)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (midPizza.checked) {
        priceTopings.innerText = `${(tPizza += 52)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (bigPizza.checked) {
        priceTopings.innerText = `${(tPizza += 57)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      return false;
    }

    if (choseIngredient == telya.id) {
      let div = document.createElement("img");
      div.src = "img/09.png";

      this.appendChild(div);

      if (smallPizza.checked) {
        priceTopings.innerText = `${(tPizza += 55)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (midPizza.checked) {
        priceTopings.innerText = `${(tPizza += 60)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (bigPizza.checked) {
        priceTopings.innerText = `${(tPizza += 65)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      return false;
    }

    if (choseIngredient == vetch1.id) {
      let div = document.createElement("img");
      div.src = "img/10.png";

      this.appendChild(div);

      if (smallPizza.checked) {
        priceTopings.innerText = `${(tPizza += 20)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (midPizza.checked) {
        priceTopings.innerText = `${(tPizza += 25)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (bigPizza.checked) {
        priceTopings.innerText = `${(tPizza += 30)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      return false;
    }

    if (choseIngredient == vetch2.id) {
      let div = document.createElement("img");
      div.src = "img/11.png";

      this.appendChild(div);

      if (smallPizza.checked) {
        priceTopings.innerText = `${(tPizza += 24)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (midPizza.checked) {
        priceTopings.innerText = `${(tPizza += 28)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      if (bigPizza.checked) {
        priceTopings.innerText = `${(tPizza += 32)}₴`;
        topings.after(priceTopings);

        priceGeneral.innerText = `${(pPizza = sPizza + tPizza)}₴`;
        price.after(priceGeneral);
      }

      return false;
    }
  });
}
makePizza();

function validateForm() {
  let btnSubmit = document.getElementsByName("btnSubmit")[0];
  let btnCancel = document.getElementsByName("cancel")[0];

  let [...fieldName] = document.getElementsByName("name");
  let [...fieldPhone] = document.getElementsByName("phone");
  let [...fieldEmail] = document.getElementsByName("email");

  let nameRegx = /^[А-яҐґЄєІіЇї]+$/g;
  let phoneRegx = /^\+380\d{9}$/g;
  let emailRegx = /^[a-z]+@[a-z]+.[a-z]+$/g;

  let nameRes, phoneRes, emailRes;

  fieldName[0].addEventListener("change", () => {
    nameRes = nameRegx.test(fieldName[0].value);

    if (nameRes) {
      fieldName[0].classList = null;
    } else {
      fieldName[0].className = "error";
    }
  });

  fieldPhone[0].addEventListener("change", () => {
    phoneRes = phoneRegx.test(fieldPhone[0].value);

    if (phoneRes) {
      fieldPhone[0].classList = null;
    } else {
      fieldPhone[0].className = "error";
    }
  });

  fieldEmail[0].addEventListener("change", () => {
    emailRes = emailRegx.test(fieldEmail[0].value);

    if (emailRes) {
      fieldEmail[0].classList = null;
    } else {
      fieldEmail[0].className = "error";
    }
  });

  btnCancel.addEventListener("click", () => {
    fieldName[0].classList = null;
    fieldPhone[0].classList = null;
    fieldEmail[0].classList = null;
  });

  btnSubmit.addEventListener("click", () => {
    if (fieldName[0].value == "") {
      fieldName[0].className = "error";
    }
    if (fieldPhone[0].value == "") {
      fieldPhone[0].className = "error";
    }
    if (fieldEmail[0].value == "") {
      fieldEmail[0].className = "error";
    }
  });
}
validateForm();

function bannerRunway() {
  let banner = document.querySelector("#banner");

  const animateMove = (element, prop, pixels) =>
    anime({
      targets: element,
      [prop]: `${pixels}px`,
      easing: "easeOutCirc",
    });

  ["mouseover", "click"].forEach(function (el) {
    banner.addEventListener(el, function (event) {
      const top = getRandomNumber(window.innerHeight - this.offsetHeight);
      const left = getRandomNumber(window.innerWidth - this.offsetWidth);

      this.style.bottom = "auto";
      this.style.right = "auto";

      animateMove(this, "left", left).play();
      animateMove(this, "top", top).play();
    });
  });

  const getRandomNumber = (num) => {
    return Math.floor(Math.random() * (num + 1));
  };
}
bannerRunway();
