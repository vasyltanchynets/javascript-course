// #1
function User(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
}

User.prototype.print = function () {
  console.log(`First name: ${this.firstName}\nLast name: ${this.lastName}`);
};

console.log("#1");

const user = new User("Jason", "Statham");
user.print();

//==========================================================================================
// #2
let wrapper = document.createElement("div");
wrapper.className = "wrapper";
document.body.prepend(wrapper);

let ul = document.createElement("ul");
wrapper.append(ul);

let li1 = document.createElement("li");
let li2 = document.createElement("li");
let li3 = document.createElement("li");
let li4 = document.createElement("li");

li1.innerText = "li 1";
li2.innerText = "li 2";
li3.innerText = "li 3";
li4.innerText = "li 4";

ul.append(li1);
ul.append(li2);
ul.append(li3);
ul.append(li4);

li1.style.backgroundColor = "#0f76dd";
li3.style.backgroundColor = "#fa1414";

const hr2 = document.createElement("hr");
wrapper.append(hr2);

document.write("<div class='div2'></div>");

//==========================================================================================
// #3
let div3 = document.createElement("div");
div3.className = "div3";

let div2 = document.querySelector(".div2");
div2.after(div3);

wrapper.append(div2);
wrapper.append(div3);

let coordinates = document.createElement("span");
coordinates.className = "coordinates";

div3.prepend(coordinates);

div3.addEventListener("mousemove", (e) => {
  coordinates.innerText = `(${e.offsetX}, ${e.offsetY})`;
  coordinates.style.left = e.clientX + 50 + "px";
  coordinates.style.top = e.clientY + "px";

  if (e.clientX > 300) {
    coordinates.style.left = e.clientX + -100 + "px";
  }

  if (e.clientY > 300) {
    coordinates.style.top = e.clientY + -30 + "px";
  }
});

const hr3 = document.createElement("hr");
wrapper.append(hr3);

document.write("<div class='div3-1'></div>");

//==========================================================================================
// #4
let btn1 = document.createElement("button");
let btn2 = document.createElement("button");
let btn3 = document.createElement("button");
let btn4 = document.createElement("button");

let div3_1 = document.querySelector(".div3-1");
div3_1.after(btn4);
div3_1.after(btn3);
div3_1.after(btn2);
div3_1.after(btn1);

wrapper.append(div3_1);
wrapper.append(btn1);
wrapper.append(btn2);
wrapper.append(btn3);
wrapper.append(btn4);

btn1.textContent = "1";
btn2.textContent = "2";
btn3.textContent = "3";
btn4.textContent = "4";

btn1.style.cursor = "pointer";
btn2.style.cursor = "pointer";
btn3.style.cursor = "pointer";
btn4.style.cursor = "pointer";

btn1.addEventListener("click", () => {
  alert("Button-1 was pressed");
});

btn2.addEventListener("click", () => {
  alert("Button-2 was pressed");
});

btn3.addEventListener("click", () => {
  alert("Button-3 was pressed");
});

btn4.addEventListener("click", () => {
  alert("Button-4 was pressed");
});

const hr4 = document.createElement("hr");
wrapper.append(hr4);

document.write("<div class='div4'></div>");

//==========================================================================================
// #5
let div5 = document.createElement("div");
div5.className = "div5";

let div4 = document.querySelector(".div4");
div4.after(div5);

wrapper.append(div4);
wrapper.append(div5);

div5.addEventListener("mouseover", () => {
  div5.style.cursor = "pointer";
  div5.style.marginLeft = 50 + "px";
});

div5.addEventListener("mouseout", () => {
  div5.style.marginLeft = 0;
});

const hr5 = document.createElement("hr");
wrapper.append(hr5);

document.write("<div class='div5-1'></div>");

//==========================================================================================
// #6
let colorInput = document.createElement("input");
colorInput.type = "color";

let div5_1 = document.querySelector(".div5-1");
div5_1.after(colorInput);

wrapper.append(div5_1);
wrapper.append(colorInput);

colorInput.addEventListener("mouseout", () => {
  document.body.style.backgroundColor = colorInput.value;
});

const hr6 = document.createElement("hr");
wrapper.append(hr6);

document.write("<div class='div6'></div>");

//==========================================================================================
// #7
let loginInput = document.createElement("input");
loginInput.placeholder = "Login";

let div6 = document.querySelector(".div6");
div6.after(loginInput);

wrapper.append(div6);
wrapper.append(loginInput);

console.log("#7");

loginInput.addEventListener("input", (e) => {
  console.log(`Login: ${e.target.value}`);
});

const hr7 = document.createElement("hr");
wrapper.append(hr7);

document.write("<div class='div7'></div>");

//==========================================================================================
// #8
let dataInput = document.createElement("input");
dataInput.placeholder = "Data";

let div7 = document.querySelector(".div7");
div7.after(dataInput);

wrapper.append(div7);
wrapper.append(dataInput);

let span = document.createElement("span");
dataInput.after(span);

dataInput.addEventListener("input", (e) => {
  span.innerText = `${e.target.value}`;
});
