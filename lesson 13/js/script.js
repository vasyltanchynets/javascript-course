let url = "https://swapi.dev/api/people";
let promise = fetch(url);

let request = promise.then((response) => {
  return response.json();
});

let people = request.then((peoples) => {
  let box = document.createElement("div");
  box.className = "box";

  peoples.results.forEach((people) => {
    const { name, gender, homeworld } = people;

    let card = document.createElement("div");
    let cardName = document.createElement("div");
    let cardGender = document.createElement("div");
    let cardHomeWorld = document.createElement("div");

    card.className = "card";
    cardName.className = "name";
    cardGender.className = "gender";
    cardHomeWorld.className = "homeworld";

    cardName.innerText = `Name: ${name}`;
    cardGender.innerText = `Gender: ${gender}`;

    let urlHomeWorld = fetch(homeworld)
      .then((response) => {
        return response.json();
      })
      .then((planets) => {
        cardHomeWorld.innerText = `Planet: ${planets.name}`;
        card.append(cardName, cardGender, cardHomeWorld);
      });

    box.append(card);
    document.querySelector("body").prepend(box);
  });
});
